import yaml
import datetime
import csv
#были использованы разные файлы yaml (сначала разбитие 120 дней - 120 файлов, последующие дни были разбиты на файлы по полмесяца)
#output
f = open('parse_yaml_final.csv', 'a')
#название таблиц в csv
fnames = ['Date', 'Flight', 'Program', 'Number', 'Class', 'Departure', 'Arrival']
#используемый файл
y = yaml.load(open("C:\\Users\\timof\\PycharmProjects\\parse_yml\\last_december.yaml"))

with f:
    #задание промежутка времени
    start = datetime.date(2017, 12, 16)
    end = datetime.date(2018, 1, 1)
    current = start
    counter = 1
    writer = csv.DictWriter(f, fieldnames=fnames)
    #запись хедеров в csv если нужно
    writer.writeheader()
    #парсинг
    while current <= end:
       # y = yaml.load(open("C:\\Users\\timof\\PycharmProjects\\parse_yml\\{}.yaml".format(counter)))(для файлов день-файл)
        date = str(current)
        item = y.get(date)
        for flight in list(item.keys()):
            departure = item.get(flight).get('FROM')
            arrival = item.get(flight).get('TO')
            tmp = item.get(flight)
            val = tmp.get('FF')
            for i in list(val.keys()):
                tmp_data = i.split(" ")
                program = tmp_data[0]
                number = tmp_data[1]
                my_class = val.get(i).get('CLASS')
                writer.writerow({'Date': date, 'Flight': flight, 'Program': program, 'Number': number,
                                 'Class': my_class, 'Departure': departure, 'Arrival': arrival})
        counter = counter + 1
        current += datetime.timedelta(days=1)
