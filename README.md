Petrov and Boshirov task
***
(Timofeev K., Kim, Matveeva, Zhitomirsky, Moskvitin)
***
Files
=====================
Files for json in json folder(.py and out csv)
-----------------------------------------------
Files for yaml in yaml folder(.py and out csv)
-----------------------------------------------
Macros for xlsx
-----------------------------------------------
***All files before upload to database was cleaned up (removed empty and useless attributes) and converted to normal view (separated Name and Surname and etc).***
-------------------------------------------------------------------------------------------------------------------------------------------------------
Transformations:
-----------------------------------------------------
*	The airlinesdata file removed fields without information.
*	Removed damaged columns from onlinetickets file
*	Where the full name was in the same field, they were divided into different fields and translated the names and surnames into Latin.
*	Both damaged columns with the document number was formed to one
*	Removed NO from the name of the flight
*	Allocated a class of service from the fare code (first letter)
----------------------------------------------------
Before analytics, create/load tables in the database:
----------------------------------------------------
*	Loaded Airport code correspondence table
*	Created table with information of passengers

