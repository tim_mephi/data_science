import json
import csv

with open('FrequentFlyerForum-Profiles.json', 'r') as fp:
    obj = json.load(fp)

f = open('parse_json.csv', 'w')
fnames = ['Number', 'Programm', 'Date', 'Departure', 'Arrival', 'Flight', 'Codeshare', 'NickName', 'Sex', 'Name',
          'Surname']
with f:
    writer = csv.DictWriter(f, fieldnames=fnames)
    writer.writeheader()
    for item in obj['Forum Profiles']:
        for program in item['Loyality Programm']:
            num_program = program['Number']
            program_lo = program['programm']
            flights = item['Registered Flights']
            for flight in flights:
                date = flight['Date']
                codeshare = flight['Codeshare']
                arrival = flight['Arrival']['Airport']
                departure = flight['Departure']['Airport']
                num_flight = flight['Flight']
                nickname = item['NickName']
                sex = item['Sex']
                name = item['Real Name']['First Name']
                surname = item['Real Name']['Last Name']
                writer.writerow({'Number': num_program, 'Programm': program_lo, 'Date': date, 'Departure': departure,
                                 'Arrival': arrival,
                                 'Flight': num_flight, 'Codeshare': codeshare, 'NickName': nickname, 'Sex': sex,
                                 'Name': name, 'Surname': surname})
f.close()
with open('parse_json.csv', 'r+') as file:
    for line in file:
        if line.rstrip():
            file.write(line)
